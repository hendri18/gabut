<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Dikala Anda Gabut</title>
        <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/ico"/>
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon"/>
        <!-- Styles -->
        <!-- Compiled and minified CSS -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script
        src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
        crossorigin="anonymous"></script>
        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </head>
    <body>
        <nav class="light-blue">
            <div class="container">
                <div class="nav-wrapper">
                    <a href="#!" class="brand-logo">DAG</a>
                    <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                    <ul class="right hide-on-med-and-down">
                        @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                        <li><a href="#">Liet</a></li>
                        <li class="dropdown">
                            <a class='dropdown-trigger' href='#' data-target='dropdown1'>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul id='dropdown1' class='dropdown-content'>
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <ul class="sidenav" id="mobile-demo">
            <li><div class="user-view">
                <div class="background">
                    <img src="https://www.atlantisbahamas.com/media/Things%20To%20Do/Water%20Park/Beaches/Hero/Experiences_Beach.jpg" style="    width: 100%;
    height: 100%;
    filter: blur(2px);">
                </div>
                <a href="#user"><img class="circle" src="https://avatars1.githubusercontent.com/u/23452885?s=400&v=4"></a>
                <a href="#name"><span class="black-text darken-4 name">{{ Auth::user()->name }}</span></a>
                <a href="#email"><span class="black-text darken-4 email">{{ Auth::user()->email }}</span></a>
            </div></li>
            @if (!Auth::guest())
            <li><a href="#!"><i class="material-icons">person</i> Profile</a></li>
            @endif
            <li><div class="divider"></div></li>
            {{-- <li><a class="subheader">Subheader</a></li> --}}
            
            @if (Auth::guest())
            <li><a href="{{ route('login') }}">Login</a></li>
            <li><a href="{{ route('register') }}">Register</a></li>
            @else
            <li><a href="#"><i class="material-icons">apps</i>Liet</a></li>
            <li class="blue-grey lighten-4">
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                   <i class="material-icons">arrow_back</i> Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
            @endif
        </ul>
        <div class="container">
            @yield('content')
        </div>
        <!-- Scripts -->
        <script type="text/javascript">
        $(document).ready(function(){
        $('.sidenav').sidenav();
        $('.dropdown-trigger').dropdown();
        });
        </script>
    </body>
</html>